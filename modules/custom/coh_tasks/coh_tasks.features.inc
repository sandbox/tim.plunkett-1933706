<?php
/**
 * @file
 * coh_tasks.features.inc
 */

/**
 * Implements hook_views_api().
 */
function coh_tasks_views_api() {
  return array("version" => "3.0");
}
