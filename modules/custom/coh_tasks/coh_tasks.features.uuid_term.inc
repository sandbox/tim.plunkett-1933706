<?php
/**
 * @file
 * coh_tasks.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function coh_tasks_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Patch code review',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '1355c9f7-7290-5be4-8d89-1f00ecf73723',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1488992',
          'title' => 'Review a Drupal core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=needs+backport+to+D7',
          'title' => 'Issues marked CNR',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>Do your own patch review and compare it to the participant\'s.</li>
<li>Review the patch in terms of the <a href="http://drupal.org/core-gates">core gates</a>.</li>
<li>See <a href="http://xjm.drupalgardens.com/review-guide#checklist">xjm\'s patch review checklist</a>.</li>
<li>The participant\'s review should be <a href="http://core.drupalofficehours.org/constructive-reviews">constructive and encouraging</a>.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>Do your own patch review and compare it to the participant\'s.</li>
<li>Review the patch in terms of the <a href="http://drupal.org/core-gates">core gates</a>.</li>
<li>See <a href="http://xjm.drupalgardens.com/review-guide#checklist">xjm\'s patch review checklist</a>.</li>
<li>The participant\'s review should be <a href="http://core.drupalofficehours.org/constructive-reviews">constructive and encouraging</a>.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/patch-code-review',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Triage and find duplicates',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '250ac86c-c939-a104-9de1-a9a637bce854',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1426582',
          'title' => 'Verify a reported issue',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?order=comment_count&sort=asc&status%5B0%5D=1&status%5B1%5D=13&status%5B2%5D=8&categories%5B0%5D=bug&version%5B0%5D=8.x&version%5B1%5D=7.x&text=&assigned=&submitted=&participant=&issue_tags_op=or&issue_tags=',
          'title' => 'Issues with 0 replies',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/triage-and-find-duplicates',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Issue summary',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '62aa7b3e-5f8b-ae74-95d7-ad07eb2ec6c7',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1427826',
          'title' => 'Write an issue summary',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+issue+summary+update',
          'title' => 'Issues tagged with "Needs issue summary update"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>Issue summaries should use the <a href="http://drupal.org/node/1155816">standard template</a>.</li>
<li>You should be able to understand the issue and its current status after reading the summary.</li>
<li>The summary should use short, clear sentences.</li>
<li>It should use markup for better readability (lists, code snippets, subheaders, etc.).</li>
<li>It should Identify the key points rather than quoting whole paragraphs.</li>
<li>It can include links to particular comments if necessary, but should not <em>only</em> link those comments.</li>
<li>The UI and API changes sections can be left as "TBD"; file followup tasks for documenting these.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>Issue summaries should use the <a href="http://drupal.org/node/1155816">standard template</a>.</li>
<li>You should be able to understand the issue and its current status after reading the summary.</li>
<li>The summary should use short, clear sentences.</li>
<li>It should use markup for better readability (lists, code snippets, subheaders, etc.).</li>
<li>It should Identify the key points rather than quoting whole paragraphs.</li>
<li>It can include links to particular comments if necessary, but should not <em>only</em> link those comments.</li>
<li>The UI and API changes sections can be left as "TBD"; file followup tasks for documenting these.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/issue-summary',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Create documentation patch',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '8e0abbc8-342e-dd44-e14e-16caf035f089',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1424598',
          'title' => 'Create a Drupal core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&component%5B%5D=documentation',
          'title' => 'Documentation issues',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/create-documentation-patch',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Manual testing',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'bf5823a5-06d6-4b74-4540-cc7c5b25d9a2',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1489010',
          'title' => 'Manually test a patch for a Drupal issue',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+manual+testing%2C+Needs+screenshot',
          'title' => 'Issues tagged with "Needs manual testing" or "Needs screenshot"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => 'The participant should:
<ul>
<li>Reproduce the issue before applying the patch.</li>
<li>Test after applying the patch to verify the fix.</li>
<li>Document the exact steps used to test the patch (a numbered list).</li>
<li>Indicate which browser/version and theme were used for testing.</li>
<li>Attach screenshots to the comment (cropped to only the relevant portion of the screen and embedded with an <code><img></code> tag).</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<p>The participant should:</p>
<ul><li>Reproduce the issue before applying the patch.</li>
<li>Test after applying the patch to verify the fix.</li>
<li>Document the exact steps used to test the patch (a numbered list).</li>
<li>Indicate which browser/version and theme were used for testing.</li>
<li>Attach screenshots to the comment (cropped to only the relevant portion of the screen and embedded with an <code>&lt;img&gt;</code> tag).</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/manual-testing',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Create patch',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'e9ac2adc-60f7-f944-2dac-046744f64ec2',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1424598',
          'title' => 'Create a Drupal core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Novice',
          'title' => 'Novice issues',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>Patches for bugfixes should be uploaded with a test-only patch followed by a combined test + fix patch.</li>
<li>Updates to earlier patches should be accompanied by an interdiff.</li>
<li>Review the patch in terms of the <a href="http://drupal.org/core-gates">core gates</a>.</li>
<li>See <a href="http://xjm.drupalgardens.com/review-guide#checklist">xjm\'s patch review checklist</a>.</li>
<li>Your review should be <a href="http://core.drupalofficehours.org/constructive-reviews">constructive and encouraging</a>.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>Patches for bugfixes should be uploaded with a test-only patch followed by a combined test + fix patch.</li>
<li>Updates to earlier patches should be accompanied by an interdiff.</li>
<li>Review the patch in terms of the <a href="http://drupal.org/core-gates">core gates</a>.</li>
<li>See <a href="http://xjm.drupalgardens.com/review-guide#checklist">xjm\'s patch review checklist</a>.</li>
<li>Your review should be <a href="http://core.drupalofficehours.org/constructive-reviews">constructive and encouraging</a>.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/create-patch',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Write tests',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'eeb8a977-e7c8-f6c4-d53d-d45545292071',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1468170',
          'title' => 'Write a Drupal core automated test',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+tests',
          'title' => 'Issues tagged with "Needs tests"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/write-tests',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Reroll',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'eed29df3-b746-9574-3d03-ad8ec44fcab4',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/patch/reroll',
          'title' => 'Re-rolling patches',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?issue_tags=Needs+reroll',
          'title' => 'Issues tagged with "Needs reroll"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>The reroll should apply and pass tests.</li>
<li>Make sure the participant rerolled the most recent patch.</li>
<li>Check the diffstat to see if the number of additions, deletions, and changed files is the same. (If they\'re different, look for why.)</li>
<li>Check to make sure all the hunks from the earlier patch are there.</li>
<li>Keep an eye out for unrelated changes that might have accidentally been rolled in.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>The reroll should apply and pass tests.</li>
<li>Make sure the participant rerolled the most recent patch.</li>
<li>Check the diffstat to see if the number of additions, deletions, and changed files is the same. (If they\'re different, look for why.)</li>
<li>Check to make sure all the hunks from the earlier patch are there.</li>
<li>Keep an eye out for unrelated changes that might have accidentally been rolled in.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/reroll',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Backport',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f1997d06-b1a6-eca4-11cd-81c1eba06964',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1538380',
          'title' => 'Backport a Drupal Core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=15&version%5B%5D=8.x&version%5B%5D=7.x',
          'title' => 'Issues with status "Patch (to be ported)',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>Compare the diffstat with the 8.x patch to double-check for missing hunks.</li>
<li>Search the <a href="http://drupal.org/list-changes/drupal">change notifications</a> for changes between D7 and D8.</li>
<li>Backports of bugfixes should be uploaded with a test-only patch followed by a combined patch.</li>
<li>Keep an eye out for API, UI, and string changes.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>Compare the diffstat with the 8.x patch to double-check for missing hunks.</li>
<li>Search the <a href="http://drupal.org/list-changes/drupal">change notifications</a> for changes between D7 and D8.</li>
<li>Backports of bugfixes should be uploaded with a test-only patch followed by a combined patch.</li>
<li>Keep an eye out for API, UI, and string changes.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/backport',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Steps to reproduce',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f4e35878-9ff2-c5d4-8d30-c989e4432653',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1468198',
          'title' => 'Document STR',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+steps+to+reproduce',
          'title' => 'Issues tagged with "Needs steps to reproduce"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>The STR should be in a numbered list.</li>
<li>Ideally, the STR should start from (e.g.) "Install Drupal 8.x with the standard profile."</li>
<li>If the participant cannot reproduce the issue, he/she should leave a comment on the issue listing the STR he tried and add that those steps do <em>not</em> reproduce it.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>The STR should be in a numbered list.</li>
<li>Ideally, the STR should start from (e.g.) "Install Drupal 8.x with the standard profile."</li>
<li>If the participant cannot reproduce the issue, he/she should leave a comment on the issue listing the STR he tried and add that those steps do <em>not</em> reproduce it.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/steps-reproduce',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Improve patch docs/code style',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f9214c9b-8a92-0954-b18c-452ea9cdcec9',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1487976',
          'title' => 'Improve patch coding style and documentation',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=needs+backport+to+D7',
          'title' => 'Issues marked CNR or CNW',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(
      'und' => array(
        0 => array(
          'value' => '<ul>
<li>The patch should be accompanied by an interdiff.</li>
<li>Check specifically for compliance with <a href="http://drupal.org/node/1354">documentation</a> and <a href" http://drupal.org/coding-standards">coding</a> standards.</li>
<li>Comments should use clear, complete English sentences.</li>
<li>The participant is only responsible for documentation and code style. If you choose to review the full patch, separate other feedback from feedback on the cleanups.</li>
</ul>',
          'format' => 'filtered_html',
          'safe_value' => '<ul><li>The patch should be accompanied by an interdiff.</li>
<li>Check specifically for compliance with <a href="http://drupal.org/node/1354">documentation</a> and <a>coding</a> standards.</li>
<li>Comments should use clear, complete English sentences.</li>
<li>The participant is only responsible for documentation and code style. If you choose to review the full patch, separate other feedback from feedback on the cleanups.</li>
</ul>',
        ),
      ),
    ),
    'path' => array(
      'alias' => 'core-mentoring-task/improve-patch-docscode-style',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Draft change notification',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f94a99c8-9e18-0524-7541-247e8d6f8c27',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1487226',
          'title' => 'Write a change record',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+change+notification',
          'title' => 'Issues tagged with "Needs change notification"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_quick_review' => array(
      'und' => array(
        0 => array(
          'value' => '0',
        ),
      ),
    ),
    'field_notes_for_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/draft-change-notification',
      'pathauto' => FALSE,
    ),
  );
  return $terms;
}
