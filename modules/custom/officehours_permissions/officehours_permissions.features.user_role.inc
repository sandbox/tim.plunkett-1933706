<?php
/**
 * @file
 * officehours_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function officehours_permissions_user_default_roles() {
  $roles = array();

  // Exported role: facilitator.
  $roles['facilitator'] = array(
    'name' => 'facilitator',
    'weight' => '4',
  );

  // Exported role: mentor.
  $roles['mentor'] = array(
    'name' => 'mentor',
    'weight' => '3',
  );

  return $roles;
}
