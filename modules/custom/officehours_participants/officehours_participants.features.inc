<?php
/**
 * @file
 * officehours_participants.features.inc
 */

/**
 * Implements hook_views_api().
 */
function officehours_participants_views_api() {
  return array("version" => "3.0");
}
