<?php

/**
 * @file
 * Definition of officehours_task_handler_field_available.
 */

/**
 * Renders the positive 'available' value as a link.
 */
class officehours_task_handler_field_available extends entity_views_handler_field_boolean {

  /**
   * Overrides entity_views_handler_field_boolean::render().
   */
  public function render($values) {
    // Make this a link if it is available to this user.
    $this->options['alter']['make_link'] = $this->get_value($values) && user_access('create field_task_attempt');
    return parent::render($values);
  }

}
