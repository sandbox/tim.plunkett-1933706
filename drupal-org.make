core = 7.x
api = 2



; Modules
projects[admin_menu][subdir] = contrib
projects[admin_menu][download][type] = git
projects[admin_menu][download][revision] = 48a657d4e61f6eb67a891ab78130e228863f3a6e
projects[admin_menu][download][branch] = 7.x-3.x

projects[auto_nodetitle][subdir] = contrib
projects[auto_nodetitle][download][type] = git
projects[auto_nodetitle][download][revision] = 98d66fb24d54113bbdfba0d1797239493473cd78
projects[auto_nodetitle][download][branch] = 7.x-1.x

projects[ctools][subdir] = contrib
projects[ctools][download][type] = git
projects[ctools][download][revision] = 729b1284e8b5cb7092fff5c10fe52dbf02232c9f
projects[ctools][download][branch] = 7.x-1.x

projects[date][subdir] = contrib
projects[date][download][type] = git
projects[date][download][revision] = 614b4e67284df7d5ab2ca6a16f7f4a49777ab6d9
projects[date][download][branch] = 7.x-2.x
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1387890#comment-6183968
projects[date][patch][] = http://drupal.org/files/date-1387890-18.patch

projects[devel][subdir] = contrib
projects[devel][version] = 1.3

projects[diff][subdir] = contrib
projects[diff][download][type] = git
projects[diff][download][revision] = 27c9185aa0cf912f4f28eb7c0a516f49bc1dfb6c
projects[diff][download][branch] = 7.x-3.x

projects[domain][subdir] = contrib
projects[domain][download][type] = git
projects[domain][download][revision] = 4ae703e76758fde452bb6b96d48cea1b95e5c1a6
projects[domain][download][branch] = 7.x-3.x
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1427552#comment-6258524
projects[domain][patch][] = http://drupal.org/files/domain-1427552-5.patch

projects[d_o_issue_importer][subdir] = contrib
projects[d_o_issue_importer][type] = module
projects[d_o_issue_importer][download][type] = git
projects[d_o_issue_importer][download][url] = http://git.drupal.org/sandbox/xjm/1623324.git
projects[d_o_issue_importer][download][revision] = dd5b51c40cd15f3c26ecbc8d10364cf61845fa7e
projects[d_o_issue_importer][download][branch] = master
; Uncommitted code - https://drupal.org/node/2008570#comment-7517427
projects[d_o_issue_importer][patch][] = http://drupal.org/files/uncommitted_code-2008570-2.patch

projects[entity][subdir] = contrib
projects[entity][version] = 1.0-rc3

projects[entityreference][subdir] = contrib
projects[entityreference][download][type] = git
projects[entityreference][download][revision] = 0ee136101c683f5c05e8ad8a98722096abdef53b
projects[entityreference][download][branch] = 7.x-1.x

projects[extlink][subdir] = contrib
projects[extlink][download][type] = git
projects[extlink][download][revision] = 32a17e60f3f2898fa2deaf1195b1cd0233a34733
projects[extlink][download][branch] = master

projects[facetapi][subdir] = contrib
projects[facetapi][version] = 1.3

projects[features][subdir] = contrib
projects[features][version] = 1.0-rc2

projects[feeds][subdir] = contrib
projects[feeds][version] = 2.0-alpha5

projects[feeds_jsonpath_parser][subdir] = contrib
projects[feeds_jsonpath_parser][download][type] = git
projects[feeds_jsonpath_parser][download][revision] = 3b0b1ab8fc3b4d75da3946f0eaba5a19013c9095
projects[feeds_jsonpath_parser][download][branch] = 7.x-1.x
; Jsonpath library - http://drupal.org/node/2008576#comment-7479924
projects[feeds_jsonpath_parser][patch][] = http://drupal.org/files/feeds_jsonpath_parser_library-2008576-1.patch

projects[feeds_selfnode_processor][subdir] = contrib
projects[feeds_selfnode_processor][download][type] = git
projects[feeds_selfnode_processor][download][revision] = 802826d2787e6547b505deb9da5d965fdb15875b
projects[feeds_selfnode_processor][download][branch] = 7.x-1.x

projects[field_collection][subdir] = contrib
projects[field_collection][download][type] = git
projects[field_collection][download][revision] = 92a68e378707eb89b5302c6b673430a978862fc6
projects[field_collection][download][branch] = 7.x-1.x
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1239946#comment-6092064
projects[field_collection][patch][] = http://drupal.org/files/field_collection-1239946-125.patch

projects[field_collection_table][subdir] = contrib
projects[field_collection_table][download][type] = git
projects[field_collection_table][download][revision] = 669b7530615e77f4a7fdff3c02ab6e547a41add4
projects[field_collection_table][download][branch] = 7.x-1.x

projects[flag][subdir] = contrib
projects[flag][version] = 2.0-beta7

projects[git_deploy][subdir] = contrib
projects[git_deploy][download][type] = git
projects[git_deploy][download][revision] = acec4716038547fd99bb8616d37910e10beab0e6
projects[git_deploy][download][branch] = 7.x-2.x

projects[job_scheduler][subdir] = contrib
projects[job_scheduler][download][type] = git
projects[job_scheduler][download][revision] = a7ce6cccd424647cdcb1e475aea5a5582914067e
projects[job_scheduler][download][branch] = 7.x-2.x

projects[link][subdir] = contrib
projects[link][download][type] = git
projects[link][download][revision] = a1792d0a6e69c127ef280f5699307b78e68c0cf3
projects[link][download][branch] = 7.x-1.x

projects[mollom][subdir] = contrib
projects[mollom][version] = 2.6

projects[publish_button][subdir] = contrib
projects[publish_button][download][type] = git
projects[publish_button][download][revision] = c5237a406f72240dd0bb65451a9ecf175d9589c6
projects[publish_button][download][branch] = 7.x-1.x
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1696202
projects[publish_button][patch][] = http://drupal.org/files/publish_button_node_type_form.patch

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0-rc1

projects[token][subdir] = contrib
projects[token][download][type] = git
projects[token][download][revision] = 963569d1f28b2627ba7511b4cadd7fed40bde75f
projects[token][download][branch] = 7.x-1.x

projects[uuid][subdir] = contrib
projects[uuid][version] = 1.0-alpha4

projects[uuid_features][subdir] = contrib
projects[uuid_features][version] = 1.0-alpha3
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1245582#comment-5837264
; projects[uuid_features][patch][] = http://drupal.org/files/uuid_features-1245582-33.patch
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/965450#comment-5837286
; projects[uuid_features][patch][] = http://drupal.org/files/uuid_features-965450-21.patch

projects[views][subdir] = contrib
projects[views][download][type] = git
projects[views][download][revision] = 0bf210501fca1b42ae060394619e77ba88b1e0b3
projects[views][download][branch] = 7.x-3.x
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1591678#comment-6104198
projects[views][patch][] = http://drupal.org/files/views-1591678-13.patch

projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][download][type] = git
projects[views_bulk_operations][download][revision] = b0ac95b6aea09755c8ad2d46035487d67d327615
projects[views_bulk_operations][download][branch] = 7.x-3.x

projects[views_field_view][subdir] = contrib
projects[views_field_view][version] = 1.1

projects[views_litepager][subdir] = contrib
projects[views_litepager][version] = 3.0



; Libraries
libraries[SolrPhpClient][download][type] = get
libraries[SolrPhpClient][download][url] = https://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.tgz
