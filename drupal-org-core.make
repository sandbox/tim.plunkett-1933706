api = 2
core = 7.x



; Core
projects[drupal][type] = core
projects[drupal][download][type] = git
projects[drupal][download][revision] = f7b54a48e964fc3689de58cf0b07d55e4b0e2b98
projects[drupal][download][branch] = 7.x
; @TODO - DOCUMENT THIS PATCH - http://drupal.org/node/1325628#comment-6119884
projects[drupal][patch][] = http://drupal.org/files/efq_prefix-132562-100-d7-do_not_test.patch
